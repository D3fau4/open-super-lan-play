﻿using System;
using System.Net;
using System.Net.Cache;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Linq;
using WindowsFormsApp2.Clases;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!File.Exists("MaterialSkin.dll"))
            {
                FileStream fsArchivo = new FileStream("MaterialSkin.dll", FileMode.Create);
                fsArchivo.Write(Properties.Resources.MaterialSkin, 0, Properties.Resources.MaterialSkin.Length);
                fsArchivo.Close();
            }

            String thisprocessname = Process.GetCurrentProcess().ProcessName;

            if (Process.GetProcesses().Count(p => p.ProcessName == thisprocessname) > 1)
            {
                MessageBox.Show("El cliente ya se encuentra abierto.");
                return;
            }

            if (!System.AppDomain.CurrentDomain.FriendlyName.Equals("Super-lan-play-client.exe"))
            {
                File.Copy(System.AppDomain.CurrentDomain.FriendlyName, "Super-lan-play-client.exe", true);
                System.Diagnostics.Process splc = new System.Diagnostics.Process();
                splc.StartInfo = new System.Diagnostics.ProcessStartInfo("Super-lan-play-client.exe");
                splc.Start();
            }
            else
            {
                if (File.Exists("Super-lan-play-client.temp.exe"))
                {
                    File.Delete("Super-lan-play-client.temp.exe");
                }
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                verificarIdiomas(CultureInfo.CurrentCulture.Name.Split('-')[0]);
                Lanzador.lanzar();
            }
        }

        public static void verificarIdiomas(string culture, bool verificacion = true)
        {
            string responseFromServer = string.Empty;
            var webrequest = (HttpWebRequest)WebRequest.Create(
                string.Format("https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/Lenguages/slp_{0}.json",
                    culture));
            webrequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            webrequest.Method = verificacion ? WebRequestMethods.Http.Head : WebRequestMethods.Http.Get;

            try
            {
                using (WebResponse response = webrequest.GetResponse())
                {
                    using (Stream stream2 = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream2))
                        {
                            responseFromServer = reader.ReadToEnd();
                            reader.Close();
                        }
                        stream2.Close();
                    }
                    response.Close();
                }

                if (!string.IsNullOrWhiteSpace(responseFromServer))
                {
                    putLanguage(stdClassCSharp.jsonToStdClass(responseFromServer));
                }
                if (verificacion)
                {
                    verificarIdiomas(culture, false);
                }
            }
            catch
            {
                if(!culture.Equals("en"))
                    verificarIdiomas("en", false);
            }
        }

        private static void putLanguage(stdClassCSharp jsonLanguages)
        {
            try
            {
                string cadenaLenguage = string.Empty;
                stdClassCSharp formPrincipal = jsonLanguages["principalScreen"];
                LlenarPrincipalDiseno(formPrincipal["design"] as stdClassCSharp);
                LlenarPrincipalMensajes(formPrincipal["infoMessages"] as stdClassCSharp);
                LlenarBanMensajes(jsonLanguages["banInfoMessages"] as stdClassCSharp);
                LlenarCGNATInfo(jsonLanguages["CGNAT-screen"] as stdClassCSharp);
                LlenarPantallaMensajes(jsonLanguages["messageScreen"] as stdClassCSharp);
            }
            catch
            {
            }
        }

        private static void LlenarPrincipalDiseno(stdClassCSharp diseno)
        {
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.BotonConectar = diseno["BotonConectar"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.BotonDesconectar = diseno["BotonDesconectar"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.DescargasLabel = diseno["DescargasLabel"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.EquipoSuperLabel = diseno["EquipoSuperLabel"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.GuardarLogCheck = diseno["GuardarLogCheck"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.SeleccionarInterfazCheck = diseno["SeleccionarInterfazCheck"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TabAjustes = diseno["TabAjustes"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TabCreditos = diseno["TabCreditos"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TabUtilidades = diseno["TabUtilidades"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TemaLabel = diseno["TemaLabel"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.labelLenguage = diseno["labelLenguage"];
            Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TraducedBy = diseno["TraducedBy"];
        }

        private static void LlenarPrincipalMensajes(stdClassCSharp mensajes)
        {
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.AccederPrimeroALaWeb = mensajes["AccederPrimeroALaWeb"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.DesconectarDeSwitchPrimero = mensajes["DesconectarDeSwitchPrimero"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.DesconectarseDePS4Primero = mensajes["DesconectarseDePS4Primero"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.DesinstalaWinPacp = mensajes["DesinstalaWinPacp"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.ErrorAlConectarLanPlay = mensajes["ErrorAlConectarLanPlay"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.ErrorDescargaIntentarDeNuevo = mensajes["ErrorDescargaIntentarDeNuevo"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.ErrorLanzarLanPlay = mensajes["ErrorLanzarLanPlay"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.ErrorValidarAcceso = mensajes["ErrorValidarAcceso"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.ErrorValidarConfiguracion = mensajes["ErrorValidarConfiguracion"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.EstatusConectandoConServidor = mensajes["EstatusConectandoConServidor"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.EstatusDescargandoLanPLay = mensajes["EstatusDescargandoLanPLay"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.EstatusLanzandoLanPlay = mensajes["EstatusLanzandoLanPlay"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.FalloInstalacion = mensajes["FalloInstalacion"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.InstalarNpacp = mensajes["InstalarNpacp"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.MensajeVersionBeta = mensajes["MensajeVersionBeta"];
            Lenguages.FormPrincipal.Messages.StringPrincipalMessages.MensajeVersionNormal = mensajes["MensajeVersionNormal"];
        }

        private static void LlenarBanMensajes(stdClassCSharp banMensajes)
        {
            Lenguages.FormSuperBan.StringSuperBan.SuperBan = banMensajes["SuperBan"];
            Lenguages.FormSuperBan.StringSuperBan.TemporalBan = banMensajes["TemporalBan"];
        }

        private static void LlenarCGNATInfo(stdClassCSharp CGNAT)
        {
            Lenguages.CGNATtest.StringCGNAT.BotonIniciar = CGNAT["BotonIniciar"];
            Lenguages.CGNATtest.StringCGNAT.BotonVolver = CGNAT["BotonVolver"];
            Lenguages.CGNATtest.StringCGNAT.OperadorConCGNat = CGNAT["OperadorConCGNat"];
            Lenguages.CGNATtest.StringCGNAT.OperadorNoCGNat = CGNAT["OperadorNoCGNat"];
            Lenguages.CGNATtest.StringCGNAT.PruebaCgNat = CGNAT["PruebaCgNat"];
        }

        private static void LlenarPantallaMensajes(stdClassCSharp messageScreen)
        {
            Lenguages.FormMessageBox.StringMessageBox.Boton = messageScreen["Boton"];
        }
    }
}
