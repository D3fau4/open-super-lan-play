﻿namespace WindowsFormsApp2
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.botonSwitch = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbStatus = new MaterialSkin.Controls.MaterialLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.LaboardButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.betaCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.botonPS4 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.SPLLabel = new MaterialSkin.Controls.MaterialLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.checkLog = new MaterialSkin.Controls.MaterialCheckBox();
            this.cbSeleccionarRed = new MaterialSkin.Controls.MaterialCheckBox();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialRadioButton2 = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialRadioButton1 = new MaterialSkin.Controls.MaterialRadioButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.WinPcapDownload = new MaterialSkin.Controls.MaterialRaisedButton();
            this.NpcapDownload = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.lblTraducedBy = new MaterialSkin.Controls.MaterialLabel();
            this.lblPowered = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialTabSelector2 = new MaterialSkin.Controls.MaterialTabSelector();
            this.timerObtenerIpConsola = new System.Windows.Forms.Timer(this.components);
            this.labelLenguage = new MaterialSkin.Controls.MaterialLabel();
            this.comboLenguage = new System.Windows.Forms.ComboBox();
            this.materialTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // botonSwitch
            // 
            this.botonSwitch.BackColor = System.Drawing.SystemColors.Control;
            this.botonSwitch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.botonSwitch.Depth = 0;
            this.botonSwitch.Location = new System.Drawing.Point(105, 287);
            this.botonSwitch.MouseState = MaterialSkin.MouseState.HOVER;
            this.botonSwitch.Name = "botonSwitch";
            this.botonSwitch.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.BotonConectar;
            this.botonSwitch.Primary = true;
            this.botonSwitch.Size = new System.Drawing.Size(108, 35);
            this.botonSwitch.TabIndex = 4;
            this.botonSwitch.Tag = "0";
            this.botonSwitch.UseVisualStyleBackColor = false;
            this.botonSwitch.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Depth = 0;
            this.lbStatus.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbStatus.Location = new System.Drawing.Point(12, 424);
            this.lbStatus.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(0, 19);
            this.lbStatus.TabIndex = 9;
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.BackColor = System.Drawing.SystemColors.Control;
            this.materialRaisedButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Location = new System.Drawing.Point(527, 112);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(108, 35);
            this.materialRaisedButton2.TabIndex = 33;
            this.materialRaisedButton2.Tag = "0";
            this.materialRaisedButton2.Text = "CGNAT Test";
            this.toolTip1.SetToolTip(this.materialRaisedButton2, "Abre una herramienta para validar si la conexión es por CGNat");
            this.materialRaisedButton2.UseVisualStyleBackColor = false;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click_1);
            // 
            // LaboardButton
            // 
            this.LaboardButton.BackColor = System.Drawing.SystemColors.Control;
            this.LaboardButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LaboardButton.Depth = 0;
            this.LaboardButton.Location = new System.Drawing.Point(527, 42);
            this.LaboardButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.LaboardButton.Name = "LaboardButton";
            this.LaboardButton.Primary = true;
            this.LaboardButton.Size = new System.Drawing.Size(108, 35);
            this.LaboardButton.TabIndex = 31;
            this.LaboardButton.Tag = "0";
            this.LaboardButton.Text = "Lanboard";
            this.toolTip1.SetToolTip(this.LaboardButton, "Abre la web de Super Lan Play");
            this.LaboardButton.UseVisualStyleBackColor = false;
            this.LaboardButton.Click += new System.EventHandler(this.LaboardButton_Click);
            // 
            // betaCheck
            // 
            this.betaCheck.AutoSize = true;
            this.betaCheck.Depth = 0;
            this.betaCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.betaCheck.Location = new System.Drawing.Point(12, 224);
            this.betaCheck.Margin = new System.Windows.Forms.Padding(0);
            this.betaCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.betaCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.betaCheck.Name = "betaCheck";
            this.betaCheck.Ripple = true;
            this.betaCheck.Size = new System.Drawing.Size(109, 30);
            this.betaCheck.TabIndex = 37;
            this.betaCheck.Text = "Versión Beta";
            this.toolTip1.SetToolTip(this.betaCheck, "Descarga la versión beta de Super Lan Play");
            this.betaCheck.UseVisualStyleBackColor = true;
            this.betaCheck.CheckedChanged += new System.EventHandler(this.betaCheck_CheckedChanged);
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.tabPage1);
            this.materialTabControl1.Controls.Add(this.tabPage2);
            this.materialTabControl1.Controls.Add(this.tabPage4);
            this.materialTabControl1.Controls.Add(this.tabPage5);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Location = new System.Drawing.Point(0, 69);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(653, 392);
            this.materialTabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.botonPS4);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.SPLLabel);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.botonSwitch);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(645, 366);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Lan Play";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // botonPS4
            // 
            this.botonPS4.BackColor = System.Drawing.SystemColors.Control;
            this.botonPS4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.botonPS4.Depth = 0;
            this.botonPS4.Location = new System.Drawing.Point(424, 287);
            this.botonPS4.MouseState = MaterialSkin.MouseState.HOVER;
            this.botonPS4.Name = "botonPS4";
            this.botonPS4.Primary = true;
            this.botonPS4.Size = new System.Drawing.Size(108, 35);
            this.botonPS4.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.BotonConectar;
            this.botonPS4.TabIndex = 16;
            this.botonPS4.Tag = "0";
            this.botonPS4.UseVisualStyleBackColor = false;
            this.botonPS4.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(350, 36);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(251, 226);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // SPLLabel
            // 
            this.SPLLabel.AutoSize = true;
            this.SPLLabel.Depth = 0;
            this.SPLLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.SPLLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.SPLLabel.Location = new System.Drawing.Point(544, 333);
            this.SPLLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.SPLLabel.Name = "SPLLabel";
            this.SPLLabel.Size = new System.Drawing.Size(0, 19);
            this.SPLLabel.TabIndex = 14;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(39, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 226);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.comboLenguage);
            this.tabPage2.Controls.Add(this.labelLenguage);
            this.tabPage2.Controls.Add(this.betaCheck);
            this.tabPage2.Controls.Add(this.materialLabel4);
            this.tabPage2.Controls.Add(this.checkLog);
            this.tabPage2.Controls.Add(this.materialRaisedButton2);
            this.tabPage2.Controls.Add(this.cbSeleccionarRed);
            this.tabPage2.Controls.Add(this.LaboardButton);
            this.tabPage2.Controls.Add(this.materialLabel1);
            this.tabPage2.Controls.Add(this.materialRadioButton2);
            this.tabPage2.Controls.Add(this.materialRadioButton1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TabAjustes;
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(645, 366);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(8, 131);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(55, 19);
            this.materialLabel4.TabIndex = 36;
            this.materialLabel4.Text = "Debug:";
            this.materialLabel4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkLog
            // 
            this.checkLog.AutoSize = true;
            this.checkLog.Depth = 0;
            this.checkLog.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkLog.Location = new System.Drawing.Point(12, 189);
            this.checkLog.Margin = new System.Windows.Forms.Padding(0);
            this.checkLog.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkLog.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkLog.Name = "checkLog";
            this.checkLog.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.GuardarLogCheck;
            this.checkLog.Ripple = true;
            this.checkLog.Size = new System.Drawing.Size(26, 30);
            this.checkLog.TabIndex = 34;
            this.checkLog.UseVisualStyleBackColor = true;
            // 
            // cbSeleccionarRed
            // 
            this.cbSeleccionarRed.AutoSize = true;
            this.cbSeleccionarRed.Depth = 0;
            this.cbSeleccionarRed.Font = new System.Drawing.Font("Roboto", 10F);
            this.cbSeleccionarRed.Location = new System.Drawing.Point(11, 153);
            this.cbSeleccionarRed.Margin = new System.Windows.Forms.Padding(0);
            this.cbSeleccionarRed.MouseLocation = new System.Drawing.Point(-1, -1);
            this.cbSeleccionarRed.MouseState = MaterialSkin.MouseState.HOVER;
            this.cbSeleccionarRed.Name = "cbSeleccionarRed";
            this.cbSeleccionarRed.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.SeleccionarInterfazCheck;
            this.cbSeleccionarRed.Ripple = true;
            this.cbSeleccionarRed.Size = new System.Drawing.Size(26, 30);
            this.cbSeleccionarRed.TabIndex = 32;
            this.cbSeleccionarRed.UseVisualStyleBackColor = true;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(8, 20);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TemaLabel;
            this.materialLabel1.Size = new System.Drawing.Size(0, 19);
            this.materialLabel1.TabIndex = 23;
            this.materialLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // materialRadioButton2
            // 
            this.materialRadioButton2.AutoSize = true;
            this.materialRadioButton2.Depth = 0;
            this.materialRadioButton2.Font = new System.Drawing.Font("Roboto", 10F);
            this.materialRadioButton2.Location = new System.Drawing.Point(12, 72);
            this.materialRadioButton2.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButton2.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButton2.Name = "materialRadioButton2";
            this.materialRadioButton2.Ripple = true;
            this.materialRadioButton2.Size = new System.Drawing.Size(64, 30);
            this.materialRadioButton2.TabIndex = 1;
            this.materialRadioButton2.Text = "DARK";
            this.materialRadioButton2.UseVisualStyleBackColor = true;
            this.materialRadioButton2.CheckedChanged += new System.EventHandler(this.materialRadioButton2_CheckedChanged);
            // 
            // materialRadioButton1
            // 
            this.materialRadioButton1.AutoSize = true;
            this.materialRadioButton1.Checked = true;
            this.materialRadioButton1.Depth = 0;
            this.materialRadioButton1.Font = new System.Drawing.Font("Roboto", 10F);
            this.materialRadioButton1.Location = new System.Drawing.Point(12, 42);
            this.materialRadioButton1.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButton1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButton1.Name = "materialRadioButton1";
            this.materialRadioButton1.Ripple = true;
            this.materialRadioButton1.Size = new System.Drawing.Size(68, 30);
            this.materialRadioButton1.TabIndex = 0;
            this.materialRadioButton1.TabStop = true;
            this.materialRadioButton1.Text = "LIGHT";
            this.materialRadioButton1.UseVisualStyleBackColor = true;
            this.materialRadioButton1.CheckedChanged += new System.EventHandler(this.materialRadioButton1_CheckedChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.materialLabel3);
            this.tabPage4.Controls.Add(this.WinPcapDownload);
            this.tabPage4.Controls.Add(this.NpcapDownload);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TabUtilidades;
            this.tabPage4.Size = new System.Drawing.Size(645, 366);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(281, 24);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.DescargasLabel;
            this.materialLabel3.Size = new System.Drawing.Size(0, 19);
            this.materialLabel3.TabIndex = 36;
            this.materialLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // WinPcapDownload
            // 
            this.WinPcapDownload.BackColor = System.Drawing.SystemColors.Control;
            this.WinPcapDownload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.WinPcapDownload.Depth = 0;
            this.WinPcapDownload.Location = new System.Drawing.Point(267, 118);
            this.WinPcapDownload.MouseState = MaterialSkin.MouseState.HOVER;
            this.WinPcapDownload.Name = "WinPcapDownload";
            this.WinPcapDownload.Primary = true;
            this.WinPcapDownload.Size = new System.Drawing.Size(108, 35);
            this.WinPcapDownload.TabIndex = 35;
            this.WinPcapDownload.Tag = "0";
            this.WinPcapDownload.Text = "WINPCAP";
            this.WinPcapDownload.UseVisualStyleBackColor = false;
            this.WinPcapDownload.Click += new System.EventHandler(this.WinPcapDownload_Click);
            // 
            // NpcapDownload
            // 
            this.NpcapDownload.BackColor = System.Drawing.SystemColors.Control;
            this.NpcapDownload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NpcapDownload.Depth = 0;
            this.NpcapDownload.Location = new System.Drawing.Point(267, 57);
            this.NpcapDownload.MouseState = MaterialSkin.MouseState.HOVER;
            this.NpcapDownload.Name = "NpcapDownload";
            this.NpcapDownload.Primary = true;
            this.NpcapDownload.Size = new System.Drawing.Size(108, 35);
            this.NpcapDownload.TabIndex = 34;
            this.NpcapDownload.Tag = "0";
            this.NpcapDownload.Text = "Npcap";
            this.NpcapDownload.UseVisualStyleBackColor = false;
            this.NpcapDownload.Click += new System.EventHandler(this.NpcapDownload_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.lblTraducedBy);
            this.tabPage5.Controls.Add(this.lblPowered);
            this.tabPage5.Controls.Add(this.materialLabel2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TabCreditos;
            this.tabPage5.Size = new System.Drawing.Size(645, 366);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // lblTraducedBy
            // 
            this.lblTraducedBy.AutoSize = true;
            this.lblTraducedBy.Depth = 0;
            this.lblTraducedBy.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblTraducedBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTraducedBy.Location = new System.Drawing.Point(214, 180);
            this.lblTraducedBy.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblTraducedBy.Name = "lblTraducedBy";
            this.lblTraducedBy.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.TraducedBy;
            this.lblTraducedBy.Size = new System.Drawing.Size(0, 19);
            this.lblTraducedBy.TabIndex = 27;
            // 
            // lblPowered
            // 
            this.lblPowered.AutoSize = true;
            this.lblPowered.Depth = 0;
            this.lblPowered.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblPowered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPowered.Location = new System.Drawing.Point(470, 333);
            this.lblPowered.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblPowered.Name = "lblPowered";
            this.lblPowered.Size = new System.Drawing.Size(165, 19);
            this.lblPowered.TabIndex = 26;
            this.lblPowered.Text = "Powered lan-play v0.0.5";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(214, 40);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.EquipoSuperLabel;
            this.materialLabel2.Size = new System.Drawing.Size(0, 19);
            this.materialLabel2.TabIndex = 24;
            this.materialLabel2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // materialTabSelector2
            // 
            this.materialTabSelector2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialTabSelector2.BaseTabControl = this.materialTabControl1;
            this.materialTabSelector2.Depth = 0;
            this.materialTabSelector2.Location = new System.Drawing.Point(94, 25);
            this.materialTabSelector2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector2.Name = "materialTabSelector2";
            this.materialTabSelector2.Size = new System.Drawing.Size(653, 38);
            this.materialTabSelector2.TabIndex = 15;
            this.materialTabSelector2.Text = "materialTabSelector2";
            // 
            // timerObtenerIpConsola
            // 
            this.timerObtenerIpConsola.Interval = 30000;
            this.timerObtenerIpConsola.Tick += new System.EventHandler(this.timerObtenerIpConsola_Tick);
            // 
            // labelLenguage
            // 
            this.labelLenguage.AutoSize = true;
            this.labelLenguage.Depth = 0;
            this.labelLenguage.Font = new System.Drawing.Font("Roboto", 11F);
            this.labelLenguage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelLenguage.Location = new System.Drawing.Point(285, 263);
            this.labelLenguage.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelLenguage.Name = "labelLenguage";
            this.labelLenguage.Size = new System.Drawing.Size(85, 19);
            this.labelLenguage.TabIndex = 38;
            this.labelLenguage.Text = Lenguages.FormPrincipal.Design.StringsPrincipalDesign.labelLenguage;
            this.labelLenguage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // comboLenguage
            // 
            this.comboLenguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboLenguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboLenguage.FormattingEnabled = true;
            this.comboLenguage.Location = new System.Drawing.Point(247, 285);
            this.comboLenguage.Name = "comboLenguage";
            this.comboLenguage.Size = new System.Drawing.Size(170, 21);
            this.comboLenguage.TabIndex = 39;
            this.comboLenguage.SelectedIndexChanged += new System.EventHandler(this.comboLenguage_SelectedIndexChanged);
            // 
            // FormPrincipal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(651, 461);
            this.Controls.Add(this.materialTabControl1);
            this.Controls.Add(this.materialTabSelector2);
            this.Controls.Add(this.lbStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.materialTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialRaisedButton botonSwitch;
        private MaterialSkin.Controls.MaterialLabel lbStatus;
        private System.Windows.Forms.ToolTip toolTip1;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector2;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButton2;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButton1;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialRaisedButton LaboardButton;
        private MaterialSkin.Controls.MaterialCheckBox cbSeleccionarRed;
        private MaterialSkin.Controls.MaterialLabel SPLLabel;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialCheckBox checkLog;
        private MaterialSkin.Controls.MaterialRaisedButton WinPcapDownload;
        private MaterialSkin.Controls.MaterialRaisedButton NpcapDownload;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel lblPowered;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private MaterialSkin.Controls.MaterialRaisedButton botonPS4;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialCheckBox betaCheck;
        private System.Windows.Forms.Timer timerObtenerIpConsola;
        private MaterialSkin.Controls.MaterialLabel lblTraducedBy;
        private System.Windows.Forms.ComboBox comboLenguage;
        private MaterialSkin.Controls.MaterialLabel labelLenguage;
    }
}

