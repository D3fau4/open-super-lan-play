﻿namespace WindowsFormsApp2
{
    partial class CGNATtest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbIniciada = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(34, 122);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Text = Lenguages.CGNATtest.StringCGNAT.BotonIniciar;
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(66, 25);
            this.materialRaisedButton1.TabIndex = 0;
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Location = new System.Drawing.Point(212, 122);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Text = Lenguages.CGNATtest.StringCGNAT.BotonVolver;
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(64, 25);
            this.materialRaisedButton2.TabIndex = 1;
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // lbIniciada
            // 
            this.lbIniciada.AutoSize = true;
            this.lbIniciada.BackColor = System.Drawing.Color.Transparent;
            this.lbIniciada.Depth = 0;
            this.lbIniciada.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbIniciada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbIniciada.Location = new System.Drawing.Point(41, 71);
            this.lbIniciada.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbIniciada.Name = "lbIniciada";
            this.lbIniciada.Text = Lenguages.CGNATtest.StringCGNAT.PruebaCgNat;
            this.lbIniciada.Size = new System.Drawing.Size(0, 19);
            this.lbIniciada.TabIndex = 24;
            this.lbIniciada.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbIniciada.Visible = false;
            // 
            // CGNATtest
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(301, 165);
            this.Controls.Add(this.lbIniciada);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.materialRaisedButton1);
            this.MaximizeBox = false;
            this.Name = "CGNATtest";
            this.ShowInTaskbar = false;
            this.Text = "CGNATtest";
            this.Load += new System.EventHandler(this.CGNATtest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private MaterialSkin.Controls.MaterialLabel lbIniciada;
    }
}