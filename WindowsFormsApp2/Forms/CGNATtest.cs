﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;
using MaterialSkin.Controls;
using System.Text.RegularExpressions;
using WindowsFormsApp2.Lenguages.CGNATtest;

// Simplemente realiza un tracer, en caso de haber mas de un salto daras positivo en CGNAT
// Simply make a tracer, in case there is more than one jump you will give a positive in CGNAT
namespace WindowsFormsApp2
{
    public partial class CGNATtest : MaterialForm
    {
        string pubIp = new System.Net.WebClient().DownloadString("https://api.ipify.org");
        bool cgnat = false;
        public CGNATtest()
        {
            InitializeComponent();
        }

        private void CGNATtest_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2,
                Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            lbIniciada.Visible = true;
            this.Refresh();
            Process process = new Process();

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardInput = false;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.FileName = "tracert";
            process.StartInfo.Arguments = "-h 5 -w 3000 -4 -d " + pubIp;

            process.Start();
            process.WaitForExit();
            if (process.HasExited)
            {
                string output = process.StandardOutput.ReadToEnd();

                List<string> idList = new List<string>();
                idList.AddRange(output.Split(new[] { "\r\n" }, StringSplitOptions.None));
                List<string> hops = new List<string>();
                string pattern = @"(\d+\.\d+\.\d+\.\d+)";
                for (int i = 4; i < idList.Count - 3; i++)
                {
                    Match mc = Regex.Match(idList[i], pattern);
                    if (mc.ToString() != "")
                        hops.Add(mc.ToString());
                    else
                        hops.Add("");

                }


                if (hops.Count > 2)
                    cgnat = true;
                if (hops.Count == 2 && hops[1] == pubIp)
                    cgnat = false;
                if (hops.Count < 2)
                    cgnat = false;

                if (cgnat)
                    new FormMessageBox(StringCGNAT.OperadorConCGNat, "CGNATtest");
                else
                    new FormMessageBox(StringCGNAT.OperadorNoCGNat, "CGNATtest");
            }
            lbIniciada.Visible = false;
            this.Refresh();
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }
    }
}
