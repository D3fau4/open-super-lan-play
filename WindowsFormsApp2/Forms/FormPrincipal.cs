﻿using System;
using System.IO;
using System.Net;
using MaterialSkin;
using System.Drawing;
//using Microsoft.Win32;
using System.Net.Cache;
using System.Diagnostics;
using System.Windows.Forms;
using System.Globalization;
using MaterialSkin.Controls;
using System.Net.NetworkInformation;
using WindowsFormsApp2.Lenguages.FormPrincipal.Design;
using WindowsFormsApp2.Lenguages.FormPrincipal.Messages;

namespace WindowsFormsApp2
{
    public partial class FormPrincipal : MaterialForm
    {
        const string _SERVER_SWITCH = "lanplay.retrogamer.tech";//"IPSERVERHERE";
        const string _SERVER_PS = "lanplayps.retrogamer.tech";//"2ºIPSERVERHERE";
        const string _SUPER_LAN_PLAY_VERSION = "2.0.7";
        //LAN-PLAY VERSION
        string versionActual = "0.0.6";
        string identificadorInterfaz = "";
        bool primero = true;
        bool primeroCambioIdioma = true;
        bool beta = false;
        Clases.stdClassCSharp idiomasActivos;
        HttpRequestCachePolicy noCachePolicy;
        Process bat;
        MaterialSkinManager m;
        //Obtener IP publica para la api
        string pubIp = new WebClient().DownloadString("https://api.ipify.org");

        public FormPrincipal()
        {
            InitializeComponent();
            m = MaterialSkinManager.Instance;
            m.AddFormToManage(this);
            m.Theme = MaterialSkinManager.Themes.LIGHT;
            m.ColorScheme = new ColorScheme(Primary.Grey900, Primary.Grey900, Primary.Grey900, Accent.Blue100, TextShade.WHITE);
            bat = new Process();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            noCachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            GetLenguages();
            SPLLabel.Text = "Version " + _SUPER_LAN_PLAY_VERSION + (beta ? "b" : "");
            if(beta)
            {
                betaCheck.Checked = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2,
                Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
            pictureBox1.Load("https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/Resources/super-lan-play-sw.png");
            pictureBox2.Load("https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/Resources/super-lan-play-ps.png");
            LlenarComboIdiomas();
            activaConfiguraciones(true);
            VerifyWinPcapInstalled();
            CheckVersionSuperLanPlay();
            GetActualVersion();
            ActivarReglaFirewall();
            lbStatus.BringToFront();
            liberarMemoria();
        }

        #region Inicial

        private void LlenarComboIdiomas()
        {
            bool estaMiIdioma = false;
            string miIdioma = CultureInfo.CurrentCulture.Name.Split('-')[0];
            int index = 0;
            int indexIdioma = 0;
            foreach(Clases.stdClassCSharp idioma in idiomasActivos.toArray())
            {
                if(!estaMiIdioma && miIdioma.Equals(idioma["abbreviation"] as string))
                {
                    estaMiIdioma = true;
                    indexIdioma = index;
                }
                comboLenguage.Items.Add(idioma["lenguage_name"]);
                index++;
            }

            comboLenguage.SelectedIndex = indexIdioma;
        }

        /// <summary>
        /// Agrega Super Lan Play a las reglas de firewall
        /// Add Super Lan Play to the firewall rules
        /// </summary>
        private void ActivarReglaFirewall()
        {
            try
            {
                Process netsh = new Process();
                netsh.StartInfo = new ProcessStartInfo("netsh.exe", "advfirewall firewall delete rule name=\"Super Lan Play\" dir=in");
                netsh.StartInfo.UseShellExecute = false;
                netsh.StartInfo.RedirectStandardOutput = false;
                netsh.StartInfo.RedirectStandardInput = false;
                netsh.StartInfo.CreateNoWindow = true;
                netsh.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                netsh.Start();
                netsh.WaitForExit();
                netsh.StartInfo = new ProcessStartInfo("netsh.exe", "advfirewall firewall add rule name=\"Super Lan Play\" dir=in action=allow program="+ AppDomain.CurrentDomain.FriendlyName + " enable=yes");
                netsh.StartInfo.UseShellExecute = false;
                netsh.StartInfo.RedirectStandardOutput = false;
                netsh.StartInfo.RedirectStandardInput = false;
                netsh.StartInfo.CreateNoWindow = true;
                netsh.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                netsh.Start();
            }
            catch(Exception ex)
            {
                saveLog("Ocurrió un error al activar reglas de firewall: " + ex.Message);
            }
        }

        /// <summary>
        /// Activa las ultimas configuraciones del usuario
        /// Activate the user's latest settings
        /// </summary>
        /// <param name="inicio">Valida si es inicio para activar o final para guardar</param>/Valid if it is start to activate or end to save
        private void activaConfiguraciones(bool inicio = false)
        {
            if (inicio)
            {
                int numeroConfiguracion = 0;
                if (File.Exists("Config.txt"))
                {
                    StreamReader configFile = new StreamReader("Config.txt");
                    string linea = "";
                    do
                    {
                        linea = configFile.ReadLine();
                        if (!string.IsNullOrWhiteSpace(linea))
                        {
                            switch (numeroConfiguracion)
                            {
                                case 0:
                                    if (linea.Trim().Contains("ligth"))
                                    {
                                        materialRadioButton1.Checked = true;
                                    }
                                    else
                                    {
                                        materialRadioButton2.Checked = true;
                                    }
                                    break;
                                case 1:
                                    if (linea.Trim().Contains("1"))
                                    {
                                        cbSeleccionarRed.Checked = true;
                                    }
                                    break;
                                case 2:
                                    if (linea.Trim().Contains("1"))
                                    {
                                        checkLog.Checked = true;
                                    }
                                    break;
                                case 3:
                                    comboLenguage.SelectedIndex = int.Parse(linea);
                                    break;
                            }
                            numeroConfiguracion++;
                        }
                    }
                    while (!string.IsNullOrWhiteSpace(linea));
                    configFile.Close();
                }
            }
            else
            {
                StreamWriter config = new StreamWriter("Config.txt", false);
                if (materialRadioButton1.Checked)
                {
                    config.WriteLine("ligth");
                }
                else
                {
                    config.WriteLine("dark");
                }

                if (cbSeleccionarRed.Checked)
                {
                    config.WriteLine("1");
                }
                else
                {
                    config.WriteLine("0");
                }

                if (checkLog.Checked)
                {
                    config.WriteLine("1");
                }
                else
                {
                    config.WriteLine("0");
                }
                config.WriteLine(comboLenguage.SelectedIndex);
                config.Close();
            }
        }

        /// <summary>
        /// Verifica si se encuentra instalado WinPcap, si no se encarga de ejecutar el instalador e instalarlo
        /// Verify if WinPcap is installed, if it is not responsible for running the installer and installing it
        /// </summary>
        private void VerifyWinPcapInstalled()
        {
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Npcap") && !Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\Npcap"))
            {
                if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\WinPcap") || Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\WinPcap"))
                {
                    new FormMessageBox(StringPrincipalMessages.DesinstalaWinPacp, "Super Lan Play");
                    System.Diagnostics.Process WinPcap = new System.Diagnostics.Process();
                    if (Environment.Is64BitOperatingSystem)
                    {
                        WinPcap.StartInfo = new System.Diagnostics.ProcessStartInfo(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\WinPcap\\Uninstall.exe");
                    }
                    else
                    {
                        WinPcap.StartInfo = new System.Diagnostics.ProcessStartInfo(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\WinPcap\\Uninstall.exe");
                    }
                    WinPcap.Start();
                    WinPcap.WaitForExit();
                }
                else
                {
                    new FormMessageBox(StringPrincipalMessages.InstalarNpacp, this.Text);
                }

                if (downloadFile("Npcap.exe", "https://nmap.org/npcap/dist/npcap-0.99-r7.exe"))
                {
                    try
                    {
                        execExe("Npcap.exe", "/npf_startup=yes /loopback_support=yes /dlt_null=yes /admin_only=no /dot11_support=yes /vlan_support=no /winpcap_mode=yes", true);
                    }
                    catch
                    {
                        new FormMessageBox(StringPrincipalMessages.FalloInstalacion, this.Text);
                        this.Close();
                    }
                }
            }
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Npcap") && !Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\Npcap"))
            {
                this.Close();
            }
            else
            {
                //saveLog("********Inicia verificación valores instalacion npcap********");
                //saveLog("******** Starts verification of installation values npcap ********");
                //ValideRegexNpcap();
            }
        }

        /// <summary>
        /// Inicia la verificacion de valore de npcap
        /// Start the valuation check of npcap
        /// </summary>
        //private void ValideRegexNpcap()
        //{
        //    try
        //    {
        //        RegistryKey registro = Registry.LocalMachine.OpenSubKey(
        //                "SYSTEM\\CurrentControlSet\\Services\\npf\\Parameters", false);
        //
        //        string[] registrosInstalados = registro.GetValueNames();
        //
        //        for (int i = 0; i < registrosInstalados.Length; i++)
        //        {
        //            if (registrosInstalados[i].Contains("DltNull") || registrosInstalados[i].Contains("Dot11Support")
        //                || registrosInstalados[i].Contains("LoopbackSupport") || registrosInstalados[i].Contains("WinPcapCompatible"))
        //            {
        //                saveLog(registrosInstalados[i] + ": " + registro.GetValue(registrosInstalados[i]));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        saveLog("Ocurrió un error al verificar la instalación de npcap: " + ex.Message);
        //    }
        //
        //    saveLog("********Termina verificación valores instalacion npcap********");
        //    saveLog("********Ends verification of installation values npcap********");
        //}

        //Comprueba la version del cliente
        // Check the client's version
        private void CheckVersionSuperLanPlay(bool forzarDescarga = false)
        {
            string responseFromServer = string.Empty;
            var webrequest = (HttpWebRequest)WebRequest.Create(beta ? "https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/SLP-version-beta.dat" :
                "https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/SLP-version.dat");
            webrequest.CachePolicy = noCachePolicy;
            webrequest.Method = WebRequestMethods.Http.Get;

            try
            {
                using (WebResponse response = webrequest.GetResponse())
                {
                    using (System.IO.Stream stream2 = response.GetResponseStream())
                    {
                        using (System.IO.StreamReader reader = new System.IO.StreamReader(stream2))
                        {
                            responseFromServer = reader.ReadToEnd();
                            reader.Close();
                        }
                        stream2.Close();
                    }
                    response.Close();
                }

                if (!string.IsNullOrWhiteSpace(responseFromServer))
                {
                    string[] versionLocal = _SUPER_LAN_PLAY_VERSION.Split('.');
                    string[] versionRemota = responseFromServer.Trim().Split('.');
                    if ((Convert.ToInt32(versionRemota[0]) > Convert.ToInt32(versionLocal[0]) ||
                        (Convert.ToInt32(versionRemota[0]) == Convert.ToInt32(versionLocal[0]) && Convert.ToInt32(versionRemota[1]) > Convert.ToInt32(versionLocal[1]))
                        || (Convert.ToInt32(versionRemota[0]) == Convert.ToInt32(versionLocal[0]) && Convert.ToInt32(versionRemota[1]) == Convert.ToInt32(versionLocal[1])
                            && Convert.ToInt32(versionRemota[2]) > Convert.ToInt32(versionLocal[2]))) || forzarDescarga)
                    {
                        UpdateSuperLanPlay();
                    }
                }
            }
            catch
            {
            }
        }
        //Descarga la update
        //Download the update
        private void UpdateSuperLanPlay()
        {
            if (downloadFile("Super-lan-play-client.temp.exe", beta ? "https://github.com/D3fau4/Super-Lan-Play/raw/master/Super-lan-play-client-b.exe" :
                "https://github.com/D3fau4/Super-Lan-Play/raw/master/Super-lan-play-client.exe"))
            {
                Process slpc = new Process();
                slpc.StartInfo = new ProcessStartInfo("Super-lan-play-client.temp.exe");
                slpc.Start();
                this.Close();
            }
        }

        /// <summary>
        /// Consulta la version actual utilizada de lan-play
        /// </summary>
        private void GetActualVersion()
        {
            if (File.Exists("versionActual.dat"))
            {
                StreamReader srArchivo = new StreamReader("versionActual.dat");
                string leer = srArchivo.ReadLine();
                if (!string.IsNullOrWhiteSpace(leer))
                {
                    versionActual = leer;
                }
            }
        }

        #endregion

        #region metodos varios

        private void ActivarDesactivarControles(bool accion)
        {
            botonSwitch.Enabled = accion;
            botonPS4.Enabled = accion;
        }

         private void AbrirLanboard(string plataforma = "")
         {
         Process lanboard = new Process();
           if (plataforma.Equals(""))
          {
          lanboard.StartInfo = new ProcessStartInfo("http://lanboard.retrogamer.tech");
        }
        //  else
        //   {
        //  lanboard.StartInfo = new ProcessStartInfo(
        //string.Format("http://lanboard.retrogamer.tech/APIHERE/CHECKHERE", plataforma));
        //}
        //lanboard.Start();
    }

    #endregion

    private void comboLenguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            string textoIdiomaCambiar = string.Empty;
            if (!primeroCambioIdioma)
            {
                Clases.stdClassCSharp idiomaCambiar = idiomasActivos[comboLenguage.Items[comboLenguage.SelectedIndex].ToString().ToLower()] as Clases.stdClassCSharp;
                textoIdiomaCambiar = idiomaCambiar["abbreviation"];
                Program.verificarIdiomas(textoIdiomaCambiar);
                CambiarIdioma();
            }
            primeroCambioIdioma = false;
        }

        private void CambiarIdioma()
        {
            if(Convert.ToInt16(botonSwitch.Tag).Equals(0))
            {
                botonSwitch.Text = StringsPrincipalDesign.BotonConectar;
            }
            else
            {
                botonSwitch.Text = StringsPrincipalDesign.BotonDesconectar;
            }
            if (Convert.ToInt16(botonPS4.Tag).Equals(0))
            {
                botonPS4.Text = StringsPrincipalDesign.BotonConectar;
            }
            else
            {
                botonPS4.Text = StringsPrincipalDesign.BotonDesconectar;
            }
            tabPage2.Text = StringsPrincipalDesign.TabAjustes;
            materialLabel1.Text = StringsPrincipalDesign.TemaLabel;
            cbSeleccionarRed.Text = StringsPrincipalDesign.SeleccionarInterfazCheck;
            checkLog.Text = StringsPrincipalDesign.GuardarLogCheck;
            labelLenguage.Text = StringsPrincipalDesign.labelLenguage;
            tabPage4.Text = StringsPrincipalDesign.TabUtilidades;
            materialLabel3.Text = StringsPrincipalDesign.DescargasLabel;
            tabPage5.Text = StringsPrincipalDesign.TabCreditos;
            materialLabel2.Text = StringsPrincipalDesign.EquipoSuperLabel;
            lblTraducedBy.Text = StringsPrincipalDesign.TraducedBy;
            materialTabSelector2.Refresh();
        }

        //Inicia el lan-play e seconecta al servidor
        //Start the lan-play and connect to the server
        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            bool ejecutar = true;
            ActivarDesactivarControles(false);
            if (Convert.ToInt32(botonSwitch.Tag).Equals(0))
            {
                if (Convert.ToInt32(botonPS4.Tag).Equals(0))
                {
                    saveLog("****Inicia proceso de conexión a lan-play para switch****");
                    try
                    {
                        saveLog("Validando si se requiere descargar lan-play.exe");
                        if (!File.Exists("lan-play.exe") || UpdateLanPlayRequires())
                        {
                            saveLog("Es necesaria la descarga se procede a realizarse");
                            ejecutar = GenerarLanPlay();
                            saveLog("Finaliza proceso de descarga de lan play");
                        }

                        saveLog("");
                        if (ejecutar )
                        {
                            GetFunctionalDiviceId();
                            LaunchLanPlay("switch");
                        }
                        else
                        {
                            ActivarDesactivarControles(true);
                        }
                    }
                    catch (Exception ex)
                    {
                        new FormMessageBox(StringPrincipalMessages.ErrorAlConectarLanPlay, "Super Lan-Play");
                        saveLog("Ocurrió un error al conectar: " + ex.Message);
                        ActivarDesactivarControles(true);
                    }
                    saveLog("****Finaliza proceso de conexión a lan-play para switch****");
                }
                else
                {
                    new FormMessageBox(StringPrincipalMessages.DesconectarseDePS4Primero, "Super Lan-Play");
                    ActivarDesactivarControles(true);
                }
            }
            else
            {
                lanplayexit(null, null);
            }

        }
        //Inicia el lan-play e seconecta al servidor (PS4)
        //Start the lan-play and connect to the server (PS4)
        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            bool ejecutar = true;
            ActivarDesactivarControles(false);
            if (Convert.ToInt32(botonPS4.Tag).Equals(0))
            {
                if (Convert.ToInt32(botonSwitch.Tag).Equals(0))
                {
                    saveLog("****Inicia proceso de conexión a lan-play para PS4****");
                    try
                    {
                        saveLog("Validando si se requiere descargar lan-play.exe");
                        if (!File.Exists("lan-play.exe") || UpdateLanPlayRequires())
                        {
                            saveLog("Es necesaria la descarga se procede a realizarse");
                            ejecutar = GenerarLanPlay();
                            saveLog("Finaliza proceso de descarga de lan play");
                        }

                        saveLog("");
                        if (ejecutar )
                        {
                            GetFunctionalDiviceId();
                            LaunchLanPlay("ps4");
                        }
                        else
                        {
                            ActivarDesactivarControles(true);
                        }
                    }
                    catch (Exception ex)
                    {
                        new FormMessageBox(StringPrincipalMessages.ErrorAlConectarLanPlay, "Super Lan-Play");
                        saveLog("Ocurrió un error al conectar: " + ex.Message);
                        ActivarDesactivarControles(true);
                    }
                    saveLog("****Finaliza proceso de conexión a lan-play para PS4****");
                }
                else
                {
                    new FormMessageBox(StringPrincipalMessages.DesconectarDeSwitchPrimero, "Super Lan-Play");
                    ActivarDesactivarControles(true);
                }
            }
            else
            {
                lanplayexit(null, null);
            }
        }

        /// <summary>
        /// LLama el servicio de captura de paquetes cada minuto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerObtenerIpConsola_Tick(object sender, EventArgs e)
        {
            if(Convert.ToInt16(botonSwitch.Tag).Equals(1))
            {
               
                {
                    lanplayexit(null, null);
                }
                liberarMemoria();
            }
            else if(Convert.ToInt16(botonPS4.Tag).Equals(1))
            {
                
                {
                    lanplayexit(null, null);
                }
                liberarMemoria();
            }
        }

        private void HeadersChange(HttpWebRequest webrequest)
        {
            webrequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36";
            webrequest.Headers.Add("CF-Connecting-IP", pubIp);
        }
        //Valida si estas registrado en el servidor
        //Validate if you are registered on the server
        //private bool ValidateIpPublic(string plataforma)
        // {
        //     saveLog("**Inicia proceso de validacion de ip registrada**");
        // bool continuar = false;
        // string responseFromServer = string.Empty;
        //WebClient webrequest = null;
        // var webrequest = (HttpWebRequest)WebRequest.Create(
        // string.Format("http://lanboard.retrogamer.tech/APIHERE/VALIDATEIPHERE/PLATFORM", plataforma));
        // webrequest.CachePolicy = noCachePolicy;
        //     webrequest.Method = WebRequestMethods.Http.Get;
        // HeadersChange(webrequest);

        //    try
        // {

        //   using (WebResponse response = webrequest.GetResponse())
        //{
        //    using (System.IO.Stream stream2 = response.GetResponseStream())
        //{
        //   using (System.IO.StreamReader reader = new System.IO.StreamReader(stream2))
        // {
        // responseFromServer = reader.ReadToEnd();
        // reader.Close();
        //}
        // stream2.Close();
        // }
        //  response.Close();
        //}

        //if (!string.IsNullOrWhiteSpace(responseFromServer) && Convert.ToInt16(responseFromServer.Trim()) == 1)
        // {
        // saveLog("La ip fue verificada correctamente");
        //continuar = true;
        //      }
        //   else
        //{
        //   saveLog("No hay acceso a la ip, se procede a solicitar acceso");
        //continuar = false;
        //            new FormMessageBox(StringPrincipalMessages.AccederPrimeroALaWeb, "Super Lan-Play");
        //            AbrirLanboard(plataforma);
        // }
        // }
        //  catch (WebException wex)
        // {
        //continuar = false;
        //       using (var stream = wex.Response.GetResponseStream())
        //{
        //   using (var reader = new System.IO.StreamReader(stream))
        //{
        //   responseFromServer = reader.ReadToEnd();
        //}
        //}
        //  saveLog("Ha ocurrido un error al validar acceso: " + wex.Message + "\n" + responseFromServer);
        //  new FormMessageBox(StringPrincipalMessages.ErrorValidarAcceso, "Super Lan-Play");
        //}
        //    catch (Exception ex)
        // {
        // continuar = false;
        // saveLog("Ha ocurrido un error al validar acceso: " + ex.Message);
        //         new FormMessageBox(StringPrincipalMessages.ErrorValidarAcceso, "Super Lan-Play");
        // }
        // saveLog("**Termina proceso de validacion de ip registrada**");
        //saveLog("");
        //  saveLog("");
        //    return continuar;
        //}
        //Valida si la configuracion de tu consola es la correcta, en caso contrario te sacara del servidor hasta que este la configuracion correcta
        //Validate if the configuration of your console is correct, otherwise it will take you out of the server until it is the correct configuration
        // private bool ValidateIpConsole(string plataforma)
        // {
        //saveLog("**Inicia proceso de validación de configuración de consola**");
        //    bool continuar = true;
        //    string responseFromServer = string.Empty;
        //    var webrequest = (HttpWebRequest)WebRequest.Create(
        //        string.Format("http://lanboard.retrogamer.tech/APIHERE/CHECKSWLANIP",
        //        plataforma));
        //   webrequest.CachePolicy = noCachePolicy;
        // webrequest.Method = WebRequestMethods.Http.Get;
        //HeadersChange(webrequest);
        //
        //    try
        // {
        //     using (WebResponse response = webrequest.GetResponse())
        //{
        //    using (System.IO.Stream stream2 = response.GetResponseStream())
        // {
        //     using (System.IO.StreamReader reader = new System.IO.StreamReader(stream2))
        // {
        // responseFromServer = reader.ReadToEnd();
        //  reader.Close();
        // //                 }
        //  stream2.Close();
        // }
        // response.Close();
        // }

        //       if (string.IsNullOrWhiteSpace(responseFromServer) || Convert.ToInt16(responseFromServer.Trim()) == 0)
        // {
        // continuar = true;
        //  saveLog("La configuración es correcta");
        //}
        //      else if(Convert.ToInt16(responseFromServer.Trim()) == 1)
        // {
        // continuar = false;
        // saveLog("La configuración no es correcta, suspensión temporal.");
        // new FormSuperBan(1, plataforma).ShowDialog();
        // AbrirLanboard(plataforma);
        // }
        //     else if(Convert.ToInt16(responseFromServer.Trim()) == 2)
        //   {
        // continuar = false;
        // saveLog("Usuario super baneado.");
        //  new FormSuperBan(2, plataforma).ShowDialog();
        // }
        //}
        // catch (WebException wex)
        // {
        //saveLog("Ha ocurrido un error al validar tu configuración: " + wex.Message);
        //    }
        //    catch (Exception ex)
        // {
        // saveLog("Ha ocurrido un error al validar tu configuración: " + ex.Message);
        //}
        //  saveLog("**Termina proceso de validación de configuración de consola**");
        //    saveLog("");
        //    saveLog("");
        //       return continuar;
        //}


        #region ACTIVAR CLIENTE
        private bool UpdateLanPlayRequires()
        {
            bool actualizar = false;
            string responseFromServer = string.Empty;
            var webrequest = (HttpWebRequest)WebRequest.Create("https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/Version.dat");
            webrequest.CachePolicy = noCachePolicy;
            webrequest.Method = WebRequestMethods.Http.Get;

            try
            {
                using (WebResponse response = webrequest.GetResponse())
                {
                    using (System.IO.Stream stream2 = response.GetResponseStream())
                    {
                        using (System.IO.StreamReader reader = new System.IO.StreamReader(stream2))
                        {
                            responseFromServer = reader.ReadToEnd();
                            reader.Close();
                        }
                        stream2.Close();
                    }
                    response.Close();
                }

                if (!string.IsNullOrWhiteSpace(responseFromServer))
                {
                    if (!versionActual.Equals(responseFromServer.Trim()))
                    {
                        versionActual = responseFromServer.Trim();
                        actualizar = true;
                    }
                }
            }
            catch
            {
            }
            return actualizar;
        }

        /// <summary>
        /// Se encarga de descargar y genrar el archivo lan-play.exe
        /// It is responsible for downloading and generating the file lan-play.exe
        /// </summary>
        /// <returns>debuelve si se ejecuto correctamente</returns>
        /// <returns>returns if executed correctly</returns>
        private bool GenerarLanPlay()
        {
            saveLog("Descargando lan-play.exe " + versionActual + "...");
            lbStatus.Text = string.Format("{0} {1}...", StringPrincipalMessages.EstatusDescargandoLanPLay, versionActual);
            this.Refresh();
            string nombreLanPlay = "lan-play-win32.exe";
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int bytesRead = 0;
            bool cerrar = false;

            if (Environment.Is64BitOperatingSystem)
            {
                saveLog("Sistema de 64 bits para descargar lan-play-win64.exe");
                nombreLanPlay = "lan-play-win64.exe";
            }

            var webrequest = (HttpWebRequest)WebRequest.Create(
                string.Format("https://github.com/spacemeowx2/switch-lan-play/releases/download/v{0}/{1}", versionActual, nombreLanPlay));
            webrequest.CachePolicy = noCachePolicy;
            webrequest.Method = WebRequestMethods.Http.Get;
            if(File.Exists("lan-play.exe"))
            {
                File.SetAttributes("lan-play.exe", FileAttributes.Normal);
            }
            FileStream fileStream = File.Create("lan-play.exe");

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)webrequest.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        bytesRead = stream.Read(buffer, 0, bufferSize);
                        if (bytesRead == 0)
                        {
                            cerrar = true;
                        }

                        while (bytesRead != 0)
                        {
                            fileStream.Write(buffer, 0, bytesRead);
                            bytesRead = stream.Read(buffer, 0, bufferSize);
                        }
                        stream.Close();
                    }
                    response.Close();
                }
                if (cerrar)
                {
                    new FormMessageBox(StringPrincipalMessages.ErrorDescargaIntentarDeNuevo, "Super Lan-Play");
                }
                else
                {
                    if(File.Exists("versionActual.dat"))
                        File.SetAttributes("versionActual.dat", FileAttributes.Normal);

                    StreamWriter version = new StreamWriter("versionActual.dat", false);
                    version.WriteLine(versionActual);
                    version.Close();
                    lblPowered.Text = "Powered lan-play v" + versionActual;
                }
            }
            catch (WebException wex)
            {
                new FormMessageBox(StringPrincipalMessages.ErrorDescargaIntentarDeNuevo, "Super Lan-Play");
                saveLog("Error al descargar lan play: " + wex.Message);
                cerrar = true;
            }
            finally
            {
                fileStream.Close();
                if (cerrar)
                {
                    File.Delete("lan-play.exe");
                    ActivarDesactivarControles(true);
                }
            }
            return !cerrar;
        }

        /// <summary>
        /// Obtiene el identificador correcto del dispositivo de red para lan-play
        /// Get the correct identifier of the network device for lan-play
        /// </summary>
        private void GetFunctionalDiviceId()
        {
            bool continuarEthernet = true;
            saveLog("**Comienza la selección Automatica de interfaz**");
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                {
                    saveLog("Interfaz encontrada: \\Device\\NPF_" + ni.Id + " (" + ni.Name + " " + ni.Description + ")" + " [" + ip.Address.ToString() + "]");
                    if (ni.GetIPProperties().GatewayAddresses.Count > 0)
                    {
                        foreach (var gateway in ni.GetIPProperties().GatewayAddresses)
                        {
                            saveLog("GateWay de interfaz:" + gateway.Address.ToString());
                        }
                        saveLog("");
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && ni.OperationalStatus == OperationalStatus.Up && continuarEthernet)
                        {
                            saveLog("");
                            saveLog("");
                            continuarEthernet = false;
                            identificadorInterfaz = "\\Device\\NPF_" + ni.Id;
                            saveLog("Se ha seleccionado la interfaz: \\Device\\NPF_" + ni.Id + " (" + ni.Name + " " + ni.Description + ")" + " [" + ip.Address.ToString() + "]");
                            saveLog("");
                            saveLog("");
                        }
                    }
                }
            }

            saveLog("**Finaliza la selección Automatica de interfaz**\n\n");
        }

        private delegate void saveLogDelegado(string mensaje);
        /// <summary>
        /// Se encarga de guardar la respuestas en los logs si se encuentra activo
        /// It is responsible for saving the answers in the logs if it is active
        /// </summary>
        /// <param name="mensaje">Mensaje a guardar</param>
        public void saveLog(string mensaje)
        {
            if (InvokeRequired)
            {

                Invoke(new saveLogDelegado(saveLog), mensaje);
            }
            else
            {
                if (checkLog.Checked)
                {
                    StreamWriter ficheroLog;
                    if (primero)
                    {
                        ficheroLog = new StreamWriter("Log.dat", false);
                        primero = false;
                    }
                    else
                    {
                        ficheroLog = new StreamWriter("Log.dat", true);
                    }
                    ficheroLog.WriteLine(mensaje);
                    ficheroLog.Close();
                }
            }
        }

        /// <summary>
        /// Se encarga de lanzar lan-play
        /// It is responsible for launching lan-play
        /// </summary>
        private void LaunchLanPlay(string plataforma)
        {
            string parametros = string.Empty;
            saveLog("**Inicia proceso de ejecución de lan-play**");
            try
            {
                lbStatus.Text = StringPrincipalMessages.EstatusLanzandoLanPlay;
                this.Refresh();
                parametros = "--relay-server-addr " + (plataforma == "switch" ? _SERVER_SWITCH : _SERVER_PS) + ":11451";
                if (plataforma.Equals("switch"))
                {
                    parametros = parametros + " --fake-internet";
                }
                if (!string.IsNullOrWhiteSpace(identificadorInterfaz) && !cbSeleccionarRed.Checked)
                {
                    parametros = parametros + " --netif " + identificadorInterfaz;
                }
                saveLog("Ejecutando lan-play.exe de la siguiente manera:");
                saveLog("lan-play.exe " + parametros);
                bat.StartInfo = new ProcessStartInfo("lan-play.exe", parametros);
                bat.EnableRaisingEvents = true;
                bat.Exited += lanplayexit;
                lbStatus.Text = StringPrincipalMessages.EstatusConectandoConServidor;
                this.Refresh();

                if (!cbSeleccionarRed.Checked)
                {
                    bat.StartInfo.UseShellExecute = false;
                    bat.StartInfo.RedirectStandardOutput = false;
                    bat.StartInfo.RedirectStandardInput = false;
                    bat.StartInfo.CreateNoWindow = true;
                    bat.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                }
                bat.Start();

                if (plataforma.Equals("switch"))
                {
                    botonSwitch.Text = StringsPrincipalDesign.BotonDesconectar;
                    botonSwitch.Tag = 1;
                    m.ColorScheme = new ColorScheme(Primary.Red900, Primary.Red900, Primary.Red900, Accent.Red400, TextShade.WHITE);
                }
                else
                {
                    botonPS4.Text = StringsPrincipalDesign.BotonDesconectar;
                    botonPS4.Tag = 1;
                    m.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.Blue400, TextShade.WHITE);
                }
                lbStatus.Text = "";
                this.Refresh();
                ActivarDesactivarControles(true);
                timerObtenerIpConsola.Enabled = true;
            }
            catch (Exception ex)
            {
                new FormMessageBox(StringPrincipalMessages.ErrorLanzarLanPlay, "Super Lan-Play");
                saveLog("Ocurrió un error al lanzar lan-play.exe: " + ex.Message);
                ActivarDesactivarControles(true);
            }
            saveLog("**Finaliza proceso de ejecución de lan-play**");
        }

        #endregion

        private void FormPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Convert.ToInt32(botonSwitch.Tag).Equals(1) || Convert.ToInt16(botonPS4.Tag).Equals(1))
            {
                lanplayexit(null, null);
            }
            activaConfiguraciones();
        }

        private void materialRadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            m.Theme = MaterialSkinManager.Themes.DARK;
        }

        private void materialRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            m.Theme = MaterialSkinManager.Themes.LIGHT;
        }

        private void betaCheck_CheckedChanged(object sender, EventArgs e)
        {
            if(betaCheck.Checked && !beta)
            {
                beta = true;
                new FormMessageBox(StringPrincipalMessages.MensajeVersionBeta, "Super Lan Play");
                CheckVersionSuperLanPlay(true);
            }
            else if(!betaCheck.Checked)
            {
                beta = false;
                new FormMessageBox(StringPrincipalMessages.MensajeVersionNormal, "Super Lan Play");
                CheckVersionSuperLanPlay(true);
            }
            
        }

        private void LaboardButton_Click(object sender, EventArgs e)
        {
            AbrirLanboard();
        }

        private void lanplayexit(object sender, EventArgs e)
        {
            try
            {
                bat.Kill();
            }
            catch
            {
            }
            try
            {
                timerObtenerIpConsola.Enabled = false;
                botonSwitch.Text = StringsPrincipalDesign.BotonConectar;
                botonSwitch.Tag = 0;
                botonPS4.Text = StringsPrincipalDesign.BotonConectar;
                botonPS4.Tag = 0;
                m.ColorScheme = new ColorScheme(Primary.Grey900, Primary.Grey900, Primary.Grey900, Accent.Blue100, TextShade.WHITE);
                ActivarDesactivarControles(true);
            }
            catch
            {

            }
        }

        private void materialRaisedButton2_Click_1(object sender, EventArgs e)
        {
            CGNATtest frm = new CGNATtest();
            frm.ShowDialog();
        }

        private void NpcapDownload_Click(object sender, EventArgs e)
        {
            if (downloadFile("Npcap.exe", "https://nmap.org/npcap/dist/npcap-0.99-r7.exe"))
            {
                execExe("Npcap.exe", "", true);
            }
        }

        private void WinPcapDownload_Click(object sender, EventArgs e)
        {
            if (downloadFile("WinPcap.exe", "https://www.winpcap.org/install/bin/WinPcap_4_1_3.exe"))
            {
                execExe("WinPcap.exe", "", true);
            }
        }

        #region descargas sueltas
        /// <summary>
        /// Descarga el archivo de la web
        /// Download the file from the web
        /// </summary>
        /// <param name="exeDownload">Corresponde al nombre del archivo al descargarse</param>
        /// <param name="exeDownload">Corresponds to the file name when downloaded</param>
        /// <param name="url">Url donde se descargará el archivo</param>
        /// <param name="url">Url where the file will be downloaded</param>
        /// <returns></returns>
        private bool downloadFile(string exeDownload, string url, bool hidden = false)
        {
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int bytesRead = 0;
            bool cerrar = false;

            var webrequest = (HttpWebRequest)WebRequest.Create(url);
            webrequest.CachePolicy = noCachePolicy;

            webrequest.Method = WebRequestMethods.Http.Get;
            if(File.Exists(exeDownload))
                File.SetAttributes(exeDownload, FileAttributes.Normal);
            FileStream fileStream = File.Create(exeDownload);

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)webrequest.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        bytesRead = stream.Read(buffer, 0, bufferSize);
                        if (bytesRead == 0)
                        {
                            cerrar = true;
                        }

                        while (bytesRead != 0)
                        {
                            fileStream.Write(buffer, 0, bytesRead);
                            bytesRead = stream.Read(buffer, 0, bufferSize);
                        }
                        stream.Close();
                    }
                    response.Close();
                }
                if (cerrar)
                {
                    new FormMessageBox(StringPrincipalMessages.ErrorDescargaIntentarDeNuevo, "Super Lan-Play");
                }
            }
            catch
            {
                new FormMessageBox(StringPrincipalMessages.ErrorDescargaIntentarDeNuevo, "Super -Lan-Play");
                cerrar = true;
            }
            fileStream.Close();
            if(hidden)
                File.SetAttributes(exeDownload, FileAttributes.Hidden);
            if (cerrar)
            {
                File.SetAttributes(exeDownload, FileAttributes.Normal);
                File.Delete(exeDownload);
            }
            return !cerrar;
        }

        /// <summary>
        /// Ejecuta el exe seleccionado
        /// Execute the selected exe
        /// </summary>
        /// <param name="exeEjecuta">Exe a ejecutar</param>
        /// <param name="exeEjecuta">Exe to execute</param>
        /// <param name="parametros">Parametros que se le enviarán</param>
        /// <param name="parametros">Parameters that will be sent to you</param>
        /// <param name="borrarExe">Define si se borrará el archivo despues de ejecutarse</param>
        /// <param name="borrarExe">Defines if the file will be deleted after running</param>
        private void execExe(string exeEjecuta, string parametros, bool borrarExe = false)
        {
            if (File.Exists(exeEjecuta))
            {
                Process exe = new Process();
                exe.StartInfo = new ProcessStartInfo(exeEjecuta, parametros);
                exe.Start();
                exe.WaitForExit();
                if (borrarExe)
                {
                    File.SetAttributes(exeEjecuta, FileAttributes.Normal);
                    File.Delete(exeEjecuta);
                }
            }
        }

        private void GetLenguages()
        {
            string responseFromServer = string.Empty;
            var webrequest = (HttpWebRequest)WebRequest.Create("https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/lenguages.json");
            webrequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            webrequest.Method = WebRequestMethods.Http.Get;

            try
            {
                using (WebResponse response = webrequest.GetResponse())
                {
                    using (Stream stream2 = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream2))
                        {
                            responseFromServer = reader.ReadToEnd();
                            reader.Close();
                        }
                        stream2.Close();
                    }
                    response.Close();
                }

                if (!string.IsNullOrWhiteSpace(responseFromServer))
                {
                    idiomasActivos = Clases.stdClassCSharp.jsonToStdClass(responseFromServer);
                    idiomasActivos = idiomasActivos["lenguages"];
                }
            }
            catch
            {
            }
        }
        #endregion

        #region Limpieza
        /// <summary>
        /// Asigna el tamaño minimo y maximo que utiliza el proceso en ejecución
        /// Assign the minimum and maximum size used by the running process
        /// </summary>
        /// <param name="process">Proceso a asignar</param>
        /// <param name="minimumWorkingSetSize">Tamaño minimo</param>
        /// <param name="maximumWorkingSetSize">Tamaño maximo</param>
        /// <returns>Diferente de cero si asigna con exito, de lo contrario cero</returns>
        /// <returns>Different from zero if successfully assigned, otherwise zero</returns>
        [System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize",
            ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Ansi, SetLastError = true)]
        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize,
            int maximumWorkingSetSize);

        /// <summary>
        /// Libera la máxima memoria posible en la aplicacion.(El uso excesivo podria realentizar el proceso, solo uso en altos consumos de memoria)
        /// Releases the maximum possible memory in the application. (Excessive use could slow down the process, only use in high memory consumption)
        /// </summary>
        public static void liberarMemoria()
        {
            GC.Collect();
            SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle, -1, 5000);
            GC.Collect();
        }
        #endregion
    }
}