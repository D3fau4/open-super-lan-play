﻿namespace WindowsFormsApp2
{
    partial class FormMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMessageBox));
            this.labelMessage = new MaterialSkin.Controls.MaterialLabel();
            this.buttonAceptar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.BackColor = System.Drawing.Color.Transparent;
            this.labelMessage.Depth = 0;
            this.labelMessage.Font = new System.Drawing.Font("Roboto", 11F);
            this.labelMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelMessage.Location = new System.Drawing.Point(22, 76);
            this.labelMessage.MouseState = MaterialSkin.MouseState.HOVER;
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(455, 19);
            this.labelMessage.TabIndex = 25;
            this.labelMessage.Text = "Prueba para ver cuanto texto cabe en este message box sin salirse";
            this.labelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonAceptar
            // 
            this.buttonAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.buttonAceptar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAceptar.Depth = 0;
            this.buttonAceptar.Location = new System.Drawing.Point(200, 108);
            this.buttonAceptar.MouseState = MaterialSkin.MouseState.HOVER;
            this.buttonAceptar.Name = "buttonAceptar";
            this.buttonAceptar.Text = Lenguages.FormMessageBox.StringMessageBox.Boton;
            this.buttonAceptar.Primary = true;
            this.buttonAceptar.Size = new System.Drawing.Size(108, 35);
            this.buttonAceptar.TabIndex = 26;
            this.buttonAceptar.Tag = "0";
            this.buttonAceptar.UseVisualStyleBackColor = false;
            this.buttonAceptar.Click += new System.EventHandler(this.buttonAceptar_Click);
            // 
            // FormMessageBox
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(500, 155);
            this.Controls.Add(this.buttonAceptar);
            this.Controls.Add(this.labelMessage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMessageBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormMessageBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel labelMessage;
        private MaterialSkin.Controls.MaterialRaisedButton buttonAceptar;
    }
}