﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using MaterialSkin.Controls;

namespace WindowsFormsApp2
{
    public partial class FormMessageBox : MaterialForm
    {
        public FormMessageBox(string message, string titulo)
        {
            InitializeComponent();
            labelMessage.Text = message;
            if(labelMessage.Width > 455)
            {
                labelMessage.Text = fRecortaPalabrasPorSilabas(message, 65);
            }
            this.Text = titulo;
            this.ShowDialog();
        }

        private void FormMessageBox_Load(object sender, EventArgs e)
        {
            this.Width = labelMessage.Width + 45;
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2,
                Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);

            labelMessage.Location = new Point(this.Width / 2 - labelMessage.Width / 2, 76);
            buttonAceptar.Location = new Point(this.Width / 2 - buttonAceptar.Width / 2,
                labelMessage.Location.Y + labelMessage.Height + 10);
            this.Height = labelMessage.Location.Y + labelMessage.Height + buttonAceptar.Height + 20;
        }

        /// <summary>
		/// Recorta un string en silabas enviando el resto del texto hacia abajo.
		/// </summary>
		/// <param name="sTextoARecortar">Texto completo que se quiere recortar</param>
		/// <param name="maxLength">El máximo de carácteres que se requieren</param>
		/// <returns></returns>
		public string fRecortaPalabrasPorSilabas(string sTextoARecortar, int maxLength)
        {
            string sTexto = "", sTextoAEnviar = "";
            char esVocaloid = ' ';
            if (sTextoARecortar.Length > maxLength)
            {
                sTexto = sTextoARecortar.Substring(0, maxLength);
                esVocaloid = Convert.ToChar(sTextoARecortar.Substring(sTexto.Length - 1, 1).ToUpper());
                //Verificamos si no es un espacio
                if (fCalculaSiEsVocal(esVocaloid) != 2)
                {
                    if (fCalculaSiEsVocal(esVocaloid) == 0)
                    {
                        esVocaloid = Convert.ToChar(sTextoARecortar.Substring(sTexto.Length, 1).ToUpper());
                        if (fCalculaSiEsVocal(esVocaloid) == 0)
                        {
                            sTextoAEnviar = sTexto + "-\n" + sTextoARecortar.Substring(sTexto.Length);
                        }
                        else
                        {
                            sTextoAEnviar = fRecortaPalabrasPorSilabas(sTextoARecortar, maxLength - 1);
                        }
                    }
                    else
                    {
                        esVocaloid = Convert.ToChar(sTextoARecortar.Substring(sTexto.Length, 1).ToUpper());
                        if (fCalculaSiEsVocal(esVocaloid) == 0)
                        {
                            if (sTexto.Length + 2 <= sTextoARecortar.Length)
                            {
                                esVocaloid = Convert.ToChar(sTextoARecortar.Substring(sTexto.Length + 1, 1).ToUpper());
                                if (fCalculaSiEsVocal(esVocaloid) == 0)
                                {
                                    sTextoAEnviar = fRecortaPalabrasPorSilabas(sTextoARecortar, maxLength + 1);
                                }
                                else if (fCalculaSiEsVocal(esVocaloid) == 2)
                                {
                                    sTextoAEnviar = fRecortaPalabrasPorSilabas(sTextoARecortar, maxLength - 1);
                                }
                                else
                                {
                                    sTextoAEnviar = sTexto + "-\n" + sTextoARecortar.Substring(sTexto.Length);
                                }
                            }
                            else
                            {
                                sTextoAEnviar = fRecortaPalabrasPorSilabas(sTextoARecortar, maxLength - 1);
                            }
                        }
                        else
                        {
                            if (sTexto.Length + 2 <= sTextoARecortar.Length)
                            {
                                esVocaloid = Convert.ToChar(sTextoARecortar.Substring(sTexto.Length + 1, 1).ToUpper());
                                if (fCalculaSiEsVocal(esVocaloid) == 0)
                                {
                                    sTextoAEnviar = fRecortaPalabrasPorSilabas(sTextoARecortar, maxLength - 1);
                                }
                                else if (fCalculaSiEsVocal(esVocaloid) == 2)
                                {
                                    sTextoAEnviar = fRecortaPalabrasPorSilabas(sTextoARecortar, maxLength - 1);
                                }
                                else
                                {
                                    sTextoAEnviar = sTexto + "-\n" + sTextoARecortar.Substring(sTexto.Length);
                                }
                            }
                        }
                    }
                }
                else
                {
                    sTextoAEnviar = sTexto.Trim() + "\n" + sTextoARecortar.Substring(sTexto.Length);
                }
            }
            else
                sTextoAEnviar = sTextoARecortar.ToUpper();

            if (sTextoAEnviar.Contains("\n") && sTextoAEnviar.Split('\n')[1].Length > maxLength)
                sTextoAEnviar = sTextoAEnviar.Split('\n')[0] + "\n" + fRecortaPalabrasPorSilabas(sTextoAEnviar.Split('\n')[1], maxLength);
            return sTextoAEnviar;
        }

        /// <summary>
        /// Verifica si la letra es vocal.
        /// </summary>
        /// <param name="letra">La letra a válidar</param>
        /// <returns></returns>
        private int fCalculaSiEsVocal(char letra)
        {
            int EsVocal = 0;
            switch (letra)
            {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    EsVocal = 1;
                    break;
                case ' ':
                    EsVocal = 2;
                    break;
            }
            return EsVocal;
        }

        private void buttonAceptar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
