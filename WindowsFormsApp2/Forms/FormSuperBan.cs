﻿using System.Windows.Forms;
using MaterialSkin.Controls;

//Mensaje para los baneados
//Message for the banned
namespace WindowsFormsApp2
{
    public partial class FormSuperBan : MaterialForm

    {
        public FormSuperBan(int tipoBaneo, string plataforma)
        {
            InitializeComponent();
            string urlImagen = "";

            if(tipoBaneo.Equals(1))
            {
                urlImagen = string.Format("https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/Resources/temporal-{0}.png", plataforma);
                labelMessage.Text = Lenguages.FormSuperBan.StringSuperBan.TemporalBan;
                this.Text = "Temporal Ban";
            }
            else
            {
                urlImagen = string.Format("https://raw.githubusercontent.com/D3fau4/Super-Lan-Play/master/Resources/superban-{0}.gif", plataforma);
                labelMessage.Text = Lenguages.FormSuperBan.StringSuperBan.SuperBan;
                this.Text = "Super Ban";
            }

            pictureBox1.Load(urlImagen);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void buttonAceptar_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void FormSuperBan_Load(object sender, System.EventArgs e)
        {

        }
    }
}
