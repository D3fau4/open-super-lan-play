﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2.Lenguages.FormPrincipal.Messages
{
    struct StringPrincipalMessages
    {
        public static string AccederPrimeroALaWeb;
        public static string DesconectarDeSwitchPrimero;
        public static string DesconectarseDePS4Primero;
        public static string DesinstalaWinPacp;
        public static string ErrorAlConectarLanPlay;
        public static string ErrorDescargaIntentarDeNuevo;
        public static string ErrorLanzarLanPlay;
        public static string ErrorValidarAcceso;
        public static string ErrorValidarConfiguracion;
        public static string EstatusConectandoConServidor;
        public static string EstatusDescargandoLanPLay;
        public static string EstatusLanzandoLanPlay;
        public static string FalloInstalacion;
        public static string InstalarNpacp;
        public static string MensajeVersionBeta;
        public static string MensajeVersionNormal;
    }
}
