﻿namespace WindowsFormsApp2.Lenguages.FormPrincipal.Design
{
    struct StringsPrincipalDesign
    {
        public static string BotonConectar;
        public static string BotonDesconectar;
        public static string DescargasLabel;
        public static string EquipoSuperLabel;
        public static string GuardarLogCheck;
        public static string SeleccionarInterfazCheck;
        public static string TabAjustes;
        public static string TabCreditos;
        public static string TabUtilidades;
        public static string TemaLabel;
        public static string labelLenguage;
        public static string TraducedBy;
    }
}
